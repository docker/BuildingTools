FROM debian:unstable-slim
LABEL maintainer="Guillaume Charifi <guillaume.charifi@sfr.fr>"

CMD /bin/bash

ARG DEBIAN_FRONTEND=noninteractive
RUN \
        mkdir -p /usr/share/man/man1 \
                /usr/share/man/man2  \
                /usr/share/man/man3  \
                /usr/share/man/man4  \
                /usr/share/man/man5  \
                /usr/share/man/man6  \
                /usr/share/man/man7  \
                /usr/share/man/man8  \
                && \
        dpkg --add-architecture i386 && \
        apt-get -qq update && \
        apt-get upgrade -yy && \
        apt-get dist-upgrade -yy && \
        apt-get --no-install-recommends install -o APT::Immediate-Configure=false -yy \
                \
                build-essential      \
                binutils-multiarch   \
                gcc-multilib         \
                g++-multilib         \
                gcc-mingw-w64        \
                g++-mingw-w64        \
                git                  \
                pkg-config           \
                \
                ca-certificates      \
                \
                libfreetype-dev     \
                libgl-dev            \
                libglfw3-dev         \
                libssl-dev           \
                libssl-dev:i386      \
                libvulkan-dev        \
                libvulkan-dev:i386   \
                spirv-headers        \
                && \
        apt-get clean && \
        rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /build /usr/share/man/*
